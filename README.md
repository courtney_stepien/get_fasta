# README

# The goal of this project is to: 

# 1) Automatically get FASTA sequences from GenBank for my seaweed species
# 2) Reformat these FASTA sequences for phylogeny software
# 3) Sort the sequences by gene, and create one file per gene
# 4) Create a master file to inventory what genes I have for what species


How to use it: 

1) Scrape Genbank for a list of genera (takes 2-3 hours for )

python genbank_get.py taxa.txt

2) Make your database

python pilot_table.py

3) Sort the multi-gene single-species files into single-gene mutli-species files

python sort.py

4) Concatenate single-gene files that are same sequence: COI.fasta and cox1.fasta are the same gene, but named differently in GenBank

from ~/Ubuntu One/Algae/Trees/Get_FASTA/singlegene

cat 28S.fasta >> LSU.fasta 

cat 18S.fasta >> SSU.fasta

cat ribulose.fasta >> ribulose.fasta

cat cytochrome.fasta >> COI.fasta

cat cox1.fasta >> COI.fasta

rm 28S.fasta

rm 18S.fasta

rm ribulose.fasta

rm cytochrome.fasta

rm cox1.fasta

5) Format files for phylogeny software, and insert records into GenBank

python format.py 16S.fasta 16Sclean.fasta

6) Query the database as to % gene coverage

python query.py overview

python query.py genus brown

python query.py genus red

python query.py genus green

python query.py species brown

python query.py species red

python query.py species green
