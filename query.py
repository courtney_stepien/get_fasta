# Let's try to make this a function....

'''
Uses:

python query.py overview
python query.py genus brown
python query.py genus red
python query.py genus green
python query.py species brown
python query.py species red
python query.py species green

'''

# This is for querying the database

import sqlite3, csv, sys

conn = sqlite3.connect('AlgalSequences.db')
conn.text_factory = str
c0 = conn.cursor()

# Below deletes non-target genera in the database (from scraping GenBank)

genus_file = open("genus.txt", 'r')
taxa = genus_file.read().split("\n")
genus_file.close()

placeholder = '?'
placeholders = ', '.join(placeholder for unused in taxa)
query = '''DELETE FROM Genes
    WHERE GenusID NOT IN (%s)''' % placeholders
c0.execute(query, taxa)
conn.commit()

# Counting entries in the database

c0.close()

# How many genera have gene x?

def ReportGenus(taxa):
    c1 = conn.cursor()

    sp_file = open(taxa + "_genus.txt", 'r')
    spec = sp_file.read().split("\n")
    sp_file.close()
    sp = spec[0:(len(spec)-1)]
    L = len(sp)
    species = tuple(sp)
#    print sp

    placeholder = '?'
    placeholders = ', '.join(placeholder for unused in sp)
    query ='''SELECT GeneID, COUNT(DISTINCT GenusID)
        FROM Genes 
        WHERE GenusID IN (%s)
        GROUP BY GeneID''' % placeholders
    c1.execute(query, sp)

    x_tup = c1.fetchall()

    print '\nGENUS REPORT: ' + taxa + ' macroalgae\n'

    for i in range(0,len(x_tup)): 
        a = x_tup[i]
        print 'There are ' + str(a[1]) +  ' genera' + ' (' + str(100*a[1]/L) + '%) ' + 'that have ' + str(a[0])
    print '\nMissing genes have 0 entries in the database for these genera\n'
    c1.close()

############################ WORKS! FIX SPECIES QUERY BELOW


# How many species (that are in my dataset) have gene x?
# num_species doesn't work, could just import the species list and add the species I know I have, but haven't ID'd to get a total number
# species query needs to be linked to genus - because I'm getting species not in my dataset, for example 'acrosiphonia borealis' or something, b/c the species name is not unique in the dataset. TO work on another day. 


def ReportSpecies(taxa):
    
    c2 = conn.cursor()
    sp_file = open(taxa + "_sp.txt", 'r')
    spec = sp_file.read().split("\n")
    sp = spec[0:(len(spec)-1)] 
    sp_file.close()
    if taxa == 'red':
        L = 37
    elif taxa == 'brown':
        L = 8
    elif taxa == 'green':
        L = 8
    elif taxa == 'species':
        L = 53
    g_file = open(taxa + "_sp.txt", 'r')
    genus = g_file.read().split("\n")
    g_file.close()

#    print sp
    placeholder = '?'
    placeholders = ', '.join(placeholder for unused in sp)
    query = '''SELECT GeneID, COUNT(DISTINCT TaxonomyID)
        FROM Genes 
        WHERE TaxonomyID IN (%s)
        GROUP BY GeneID ''' % placeholders
    c2.execute(query, sp)
    y_tup = c2.fetchall()

    print '\nSPECIES REPORT: '+taxa + ' macroalgae' + '\n'

    for i in range(0,len(y_tup)): 
        a = y_tup[i]
        print 'There are ' + str(a[1]) +  ' species' + ' (' + str(100*a[1]/L) + '%) ' + 'that have ' + str(a[0])
    print '\nMissing genes have 0 entries in the database for these species\n'
    c2.close()

# Directing user input to the appropriate functions: 

if len(sys.argv) >1:

    if sys.argv[1] == 'overview':
        c3 = conn.cursor()        
        c3.execute('''SELECT COUNT(ID)
    FROM Genes''')
        z_tup = c3.fetchall()
        z = z_tup[0]
        print '\nThere are ' + str(z[0]) + ' entries in the database'
        ReportGenus('genus')
        ReportSpecies('species')
    elif sys.argv[1] == 'genus': 
        ReportGenus(sys.argv[2])
    elif sys.argv[1] == 'species':
        ReportSpecies(sys.argv[2])

    else:
        print '''\ninvalid entry: please enter phylo level followed by algal lineage, e.g. 'genus brown' or 'species red', or 'overview' for the full dataset' \n'''

else:
    print '''\nplease enter phylo level followed by algal lineage, e.g. 'genus brown' or 'species red', or 'overview' for the full dataset\n'''
