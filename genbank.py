import gzip, re, os, ftplib, math

class genbank:
	def __init__(self):
		self.keys = set([])
		self.types = ["bct","con","env","est","htc","htg","inv","pat","pln","pri","tsa","vrl"]
		
	def login(self):
		self.host = ftplib.FTP("ftp.bio-mirror.net")
		self.host.login("","")
		self.host.sendcmd("CWD biomirror")
		self.host.sendcmd("CWD genbank")
		
	def logout(self):
		self.host.quit()
	
	def raw_text(self,type,number):
		filename = "gb"+type.lower()+str(number)+".seq.gz"
		self.login()
		self.host.retrbinary("RETR "+filename, open(filename,"wb").write)
		file = gzip.open(filename,"rb")
		content = file.read()
		file.close()
		os.remove(filename)
		self.logout()
		return content
		
	def load(self,type,number):
		text = self.raw_text(type,number)
		text = text[text.find("\n\n\n")+3:].split("//\n")
		rec = []
		for i in text:
			this_rec = genrecord(i)
			rec.append(this_rec)
			self.keys = self.keys.union(set(this_rec.keys))
		return rec
		
	def search(self,keys, vals, types="All",numbers="All"):
		if types=="All":
			types = self.types
		elif type(types)==str:
			types = [types]
			
		if type(numbers)==int:
			numbers = [numbers]
			
		if type(vals)==str:
			vals = [vals]
			
		def write_if(i):
			for val in vals:
				if i.has_match(keys,val):
					print val, "found"
					f = open("rawgenbankdata/"+val.replace(" ","_")+".fasta", "a")
					f.write(i.fasta_form()+"\n\n")
					f.close()
					break

		for t in types:
			if numbers=="All":
				num = 1
				err = False
				while not err:
					print "searching", t, num
					try:
						[write_if(i) for i in self.load(t,num)]
						num+=1
					except ftplib.error_perm:
						err=True
			else: #if it's a list
				for num in numbers:
					print "searching", t, num
					[write_if(i) for i in self.load(t,num)]
		
class genrecord:
	def __init__(self, s):
		self.record = {"REFERENCES":[]}
		self.keys = []
		s = s.replace("\n            "," ")
		parts = re.split("\n\s{0,4}(?=[A-Z]+)", s)
		re_match = re.compile("\A([A-Z]+[_a-z]*)\s+([^$]+)")
		reference_keys = ["AUTHORS","TITLE","JOURNAL","PUBMED"]
		iter = 0
		for i in parts:
			match = re_match.match(i)
			if match==None:
				continue
				
			key = match.group(1)
			val = match.group(2)
				
			if not key in self.keys:
				self.keys.append(key)
			if key=="REFERENCE":
				self.record["REFERENCES"].append({key:val})
			elif key in reference_keys:
				self.record["REFERENCES"][-1][key] = val
			elif key=="ORIGIN":
				self.record[key] = val.translate(None," 1234567890\n\t")
			else:
				self.record[key] = val
	def get(self, key, fallback=None):
		if key in self.keys:
			return self.record[key]
		else:
			return fallback
	def has_match(self, keys, val):
		truth = False
		if type(keys)==str:
			keys = [keys]
		for i in keys:
			if re.search("(?:(?<=\W)|^)"+val+"(?=\W|$)", self.get(i,""), re.I)!=None:
				return True
		return False
	def fasta_form(self):
		top = self.get("VERSION","")
		top_match = re.match("(?P<gb>[^\s]+)\s+GI:(?P<gi>)",top)
		if top_match==None:
			out_str = ">gi||gb||"
		else:
			out_str = ">gi|"+top_match.group("gi")+"|gb|"+top_match.group("gb")+"|"
		out_str = out_str+" "+re.sub("\s+"," ",self.get("DEFINITION",""))+"\n"
		seq = self.get("ORIGIN","ERROR").upper()
		lines = int(math.floor(len(seq)/70))
		for i in range(lines):
			out_str = out_str+seq[i*70:(i+1)*70]+"\n"
		out_str = out_str+seq[lines*70:]
		return out_str

"""
#EXAMPLES FOR USE:

g = genbank()
rec = g.load("inv",1)[0]
rec.get("ORIGIN")
rec.get("REFERENCES")

rec = g.search(["DEFINITION","SOURCE","ORGANISM"], ["Synechococcus elongatus", "Xanthomonas perforans"], ["bct"], [1])
print rec[0].get("DEFINITION")
"""
