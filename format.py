"""
Example of usage:

python format.py psaA.fasta psaAclean.fasta

python format.py inputfile.fasta outputfile.fasta

"""


# This file formats single-gene files for phylo analysis

# Import the relevant libraries

import re, sqlite3, csv, sys

# create a connection to the database

conn = sqlite3.connect('AlgalSequences.db')
conn.text_factory = str

# create a 'cursor' to execute commands

# Open a file to read, then add the contents of that file to my_string

# genes = open('sequence_test_four_genes.fasta', 'r')
genes = open("singlegene/" + sys.argv[1], 'r')
my_string = genes.read()
genes.close()

# Split the string into a list of strings (a list of individual gene entries)

my_list = my_string.split('\n\n')

# Remove \n character from last string in list - or remove empty strings, depending on input file format (if straight from genbank, \n is problem, if previously sorted by my sort.py, empty string is problem)

if my_list[len(my_list)-1] == '':
    my_list = filter(None, my_list)
    print 'empty strings detected and removed'
else:
    st = my_list[len(my_list)-1]
    st = st[:-1]
    my_list[len(my_list)-1] = st
    print '\\n detected and removed from last string'

# Remove duplicates from the list of strings using a set (doesn't preserve order, but I don't care right now)

no_dupes = list(set(my_list))
a = len(my_list)
b = len(no_dupes)
c = a - b
print str(a) + ' sequences total'
print str(c) + ' duplicate sequences removed'
print str(b) + ' unique sequences in file'

# If duplicates were removed, write the new file without duplicates

if c > 0:
    f = open("singlegene/" + sys.argv[1], 'w')
    f.write('')
    f.close()
    for i in range(0, len(no_dupes)):
        gene = no_dupes[i] + "\n\n"
        f = open("singlegene/" + sys.argv[1], 'a')
        f.write(gene)
        f.close()

# Reg ex for formatting the fasta sequences

f = open("phyloformatted/" + sys.argv[2], 'a')
for i in range(0, len(no_dupes)):
    gene = re.search(r'(>).+(gb\|.+)\|(\w*)\s([\w]+)\s([\w\.]+).*\s.+([\nATGC]+)', no_dupes[i])
#    gene = re.search(r'(>).+', no_dupes[i])
# Save match.group() output to my_list
    if gene != None:
        FASTA = gene.group(1,4,5,6)

# Concatenate tuples into 1 string for the fasta file....

        cat_list = FASTA[0] + FASTA[1] + "_" + FASTA[2] + FASTA[3] + "\n\n"

# Write the FASTA string to a file
        f.write(cat_list)
f.close()

print 'DONE formatting file'


# Reg ex for getting tuple to update database 
# Might consider combining this with the previous regex for formatting

gene = sys.argv[1][0:sys.argv[1].rfind('.')]
c2 = conn.cursor()

for i in range(0, len(no_dupes)):
    rec = []
    entry = re.search(r'(>).+(gb\|.+)\|(\w*)\s([\w]+)\s([\w\.]+).*\s.+([\nATGC]+)', no_dupes[i])
# Save match.group() output to a list
    if entry != None:
        FASTA = entry.group(2,4,5,6)
        name = FASTA[1] + ' ' + FASTA[2]
#    print FASTA
# Make a tuple of the record to put into my database

        rec.append(FASTA[0])
        rec.append(FASTA[1])
        rec.append(FASTA[2])
        rec.append(name)
        rec.append(gene)
        rec.append(FASTA[3])
        record = tuple(rec)
#    print record

        c2.execute('''INSERT INTO Genes 
            (GenBankID, GenusID, SpeciesID, TaxonomyID, GeneID, Sequence) 
            VALUES (?,?,?,?,?,?)''', record)

conn.commit()
c2.close()

print 'DONE updating database'

'''
# Just below, I have to open an new connection, and probably close out the other one
c = conn.cursor()
c.execute('SELECT * FROM Genes WHERE GeneID = ?', ('16S',))
print c.fetchall()
'''
