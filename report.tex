\documentclass[12pt] {article}
\usepackage{minted}
\usepackage[margin=0.3in]{geometry}
\usepackage{upquote}
\title{Curating and Formatting Sequence Data from GenBank: Scientific Computing Project Report}
\author{Courtney C. Stepien}
\date{}
\begin{document}
    \maketitle

    \begin{abstract}
With the advent of cheaper sequencing technologies, huge amounts of DNA sequence data are being generated and published to online databases. The rate of sequence data generation far outpaces the our ability to download and curate these sequences one at a time, the most common user interface for the majority of online databases. I developed a series of Python scripts to automate the retrieval, formatting and databasing of target gene sequences for my dissertation research.
    \end{abstract}

    \section{The Problem}
GenBank and other open-access databases for DNA sequence data and phylogenetic trees are valuable resources. GenBank was first released in 1982, and the number of nucleotide bases in this database has doubled approximatedly every 18 months since then. While a fantastic resource, manually searching for and downloading target gene sequences is time-consuming and tedious. Additionally, formatting the files for analysis requires deleting new lines and inserting characters in other positions. I want to create a phylogeny of 54 seaweed species that I have been surveying in community plots over 3 years. While a relatively small project, this will not be the only time I work with gene sequence data, so automating and streamlining the process of downloading sequences from GenBank, formatting them for analysis and maintaining them in a database is a priority. 
    \begin{itemize} 
    \item[a] \textbf{Downloading sequence data}.
Currently the method for downloading sequences manually from GenBank is time-consuming. Sequences must be selected one at a time (with up to 10 displayed per page) using the website's user interface.
    \item[b] \textbf{Gene-specific files}. To analyze sequence data phylogenetically, each .fasta file must contain sequences for only 1 gene. Additionally, there is inconsistency in gene names, such that two or three names may all refer to the same target sequence. 
    \item[c] \textbf{Formatting FASTA files}. Once sequences are downloaded, they must be reformatted for phylogenetic analysis. New lines must be removed in some places, and details about the GenBankID and gene must also be removed while the species name is retained.
    \item[d] \textbf {Curating data}. Once all the sequences have been downloaded are formatted, there is still the issue of keeping track of the gene coverage for all target species. While in the past I have used Excel files, this is difficult to keep up to date manually, and is even harder to query. 
    \end{itemize}

    \section{Before and after: Raw and analysis-ready .fasta formats}
Raw downloaded .fasta take the following format:
\\

\textgreater gi\textbar \textbar gb\textbar DQ841645.1\textbar Fucus ceranoides Fc1 ribosomal protein L31 (rpl31) 

gene, partial cds; rpl31-rns intergenic spacer, complete sequence; and 16S 

ribosomal RNA (rns) gene, partial sequence; mitochondrial.

TTTAAACTTTAAAAGTAGGTACAAAAAGTTCAAGTTTTAAAACATTGTACCTTTTATATTAAG

TTTTATCGTAAATTTTAAAATATAATTAAGTGTTATTTAAAGATTTATGCATCAAATATGAGATG

ACAATAATAATTAATCTATCAATTAAAATTATTAAATAAAGTGAGTTTGATCCTGGCTCAGAG           
\\
\\
                                                                                                                                                                                          Formatted .fasta take the following format:
\\

\textgreater Fucus\_ceranoides

 TTTAAACTTTAAAAGTAGGTACAAAAAGTTCAAGTTTTAAAACATTGTACCTTTTATATTAAG

 TTTTATCGTAAATTTTAAAATATAATTAAGTGTTATTTAAAGATTTATGCATCAAATATGAGATG

 ACAATAATAATTAATCTATCAATTAAAATTATTAAATAAAGTGAGTTTGATCCTGGCTCAGAG

    \section{Methods: Downloading sequence data}
In addition to manually searching for and downloading individual sequences through the browser, GenBank allows for downloading sequence data by ftp. R.K. Lockhart provided Python code for downloading data from GenBank (\textbf{Appendix A} genbank.py and genbank\_get.py). The code takes a text file of seaweed genera as input and searches GenBank plant sequence data files for all entries that match the particular genus. Each matching sequence was then appended to an output file, separated by a new line. This code can be modified to search additional taxa besides plants. 

    \section{Methods: Creating single-gene files}
The output of the GenBank automated download is one file per genus for as many genera as are listed in the input file. For example, Acrosiphonia.txt contains all the genes in Genbank for which the genus matched Acrosiphonia. The next step is to create one file per gene that will include sequence data from all genera. In this case, the file would be named after a gene - COI.fasta - and would contain COI sequence data from all genera that have a COI sequence (Acrosiphonia, Cladophora, Mazzaella, etc.). Of the thousands of genes sequenced for my seaweed species, I am only interested in a subet of 8 genes, so I used regular expressions in Python to search for these 8 genes in each file. I used a function to search for a dictionary of gene regular expresssions (courtesy S. Allesina).
    \begin{minted}{c}
for i in range(0,len(taxa)): 
    genes = open("rawgenbankdata/" + taxa[i] + ".fasta", "r")
    my_string = genes.read()
    genes.close()

/*Split the string into a list of strings (a list of individual gene entries)*/

    my_list = my_string.split("\n\n")

/*Define variables to keep track of how many matches, mismatches*/

    match_count = 0
    mismatch_count = 0
    mismatch_index = []

    def AppendToFile(labelforfile, regularexp, sequence):
        if re.search(regularexp, sequence):
            f = open("singlegene/" + labelforfile + ".fasta", "a")
            f.write(sequence + "\n\n")
            f.close()
            print labelforfile
            global match_count
            match_count += 1

    gene_dic = {"SSU": r"\bsmall subunit\b", "rbcL": r"\brbcL\b", 
        "ribulose": r"\bribulose\b", "LSU": r"\blarge subunit ribosomal\b", 
        "28S": r"\b28S\b", "cytochrome": r"\bcytochrome\b", 
        "cox1": r"\bcox1\b", "COI": r"\bCOI\b", "tufA": r"\btufA\b", 
        "psaA": r"\bpsaA\b", "EF2": r"\bEF2\b", "16S": r"\b16S\b", 
        "18S": r"\b18S\b"}

    for L in range(len(my_list)):
        mis_match = match_count
        for gg in gene_dic:
            AppendToFile(gg, gene_dic[gg], my_list[L])
        if mis_match == match_count:
            mismatch_count += 1
            print "no match" 
            mismatch_index.append(L)

    \end{minted}
\\*Some genes are not labeled consistently. For example, genes labeled as 28S or LSU are actually the same gene; some sequence descriptions contain both labels, and some only contain one or the other label. After separating each label into it's own gene file, I concatenated the duplicate gene files using UNIX code. \\ \\*cat 28S.fasta \textgreater \textgreater LSU.fasta \\ \\* While this creates duplicate sequence entries in the concatenated file, I dealt with this by taking the set of the file in python before formatting it (the order of the sequence entries is not important).

    \section{Methods: Formatting FASTA sequences}
To format the downloaded sequence data, each gene file was split into a list of strings using the new line between each sequence. Using a regular expression, each sequence entry in the list was matched for a genus name, species name, GenBankID and sequence data (ATGC). 
    \begin{minted} {c}
f = open("phyloformatted/" + sys.argv[2], 'a')
for i in range(0, len(no_dupes)):
    gene = re.search(r"(>).+(gb\|.+)\|(\w*)\s([\w]+)\s([\w\.]+).*\s.+([\nATGC]+)", 
        no_dupes[i])

/*Save match.group() output to my_list*/

    if gene != None:
        FASTA = gene.group(1,4,5,6)

/*Concatenate tuples into 1 string for the fasta file....*/

        cat_list = FASTA[0] + FASTA[1] + "_" + FASTA[2] + FASTA[3] + "\n\n"

/*Write the FASTA string to a file*/

        f.write(cat_list)
f.close()

    \end{minted}
    \section{Methods: Creating and updating a database}
Rather than manage all this sequence data in an Excel spreadsheet, I wanted to create a database that would automatically update each time I ran the code to download and format sequences from GenBank. This way, I'd have a system where I could easily find out how many sequences I had and how many more I needed. Though I could have done this curating manually in Excel with my current project (a total of 54 species and 8 target genes per species, for 432 sequences if they have all been sequenced), I intend to do larger projects in the future, so proofing a system at this stage will save me lots of time for my next project. \\ \\I used the sqlite3 library in python to create a database. This database has only 1 main table with all the key information for each sequence. 
    \begin{minted} {c}
import sqlite3, csv

conn = sqlite3.connect("AlgalSequences.db")
conn.text_factory = str

c = conn.cursor()

c.execute('''CREATE TABLE Genes 
    (ID INTEGER PRIMARY KEY,
    GenbankID TEXT,    
    GenusID TEXT,    
    SpeciesID TEXT,
    TaxonomyID TEXT,
    GeneID TEXT, 
    Sequence TEXT)''')
conn.commit()

    \end{minted}
To update the database with new sequences, I made the update process part of the formatting process. Because the single-gene files were already read into python and converted to a list of strings (1 string = 1 sequence entry), it was easy to modify the formatting regex searches to update my database. 
    \begin{minted} {c}
c2 = conn.cursor()

for i in range(0, len(no_dupes)):
    rec = []
    entry = re.search(r"(>).+(gb\|.+)\|(\w*)\s([\w]+)\s([\w\.]+).*\s.+([\nATGC]+)",
         no_dupes[i])

/*Save match.group() output to a list*/

    if entry != None:
        FASTA = entry.group(2,4,5,6)
        name = FASTA[1] + ' ' + FASTA[2]

/*Make a tuple of the record to put into my database*/

        rec.append(FASTA[0])
        rec.append(FASTA[1])
        rec.append(FASTA[2])
        rec.append(name)
        rec.append(gene)
        rec.append(FASTA[3])
        record = tuple(rec)

        c2.execute('''INSERT INTO Genes 
            (GenBankID, GenusID, SpeciesID, TaxonomyID, GeneID, Sequence) 
            VALUES (?,?,?,?,?,?)''', record)

conn.commit()
c2.close()

    \end{minted}
    \section{Querying the database}
Once the database was updated, I created a series of scripts to query the database for details regarding the number of genera that had a specific gene, or the number of species with a certain gene. The below example is the code used for a function to ask the database what the gene coverage is for a particular genus. 
    \begin{minted} {c}

def ReportGenus(taxa):
    c1 = conn.cursor()

    sp_file = open(taxa + "_genus.txt", 'r')
    spec = sp_file.read().split("\n")
    sp_file.close()
    sp = spec[0:(len(spec)-1)]
    L = len(sp)
    species = tuple(sp)

    placeholder = '?'
    placeholders = ', '.join(placeholder for unused in sp)
    query ='''SELECT GeneID, COUNT(DISTINCT GenusID)
        FROM Genes 
        WHERE GenusID IN (%s)
        GROUP BY GeneID''' % placeholders
    c1.execute(query, sp)

    x_tup = c1.fetchall()

    \end{minted}
I also defined a similar function called ReportSpecies to count the gene coverage by species. I then used the sys library to direct user input to the appropriate function. 

    \begin{minted} {c}

if len(sys.argv) >1:

    if sys.argv[1] == "overview":
        c3 = conn.cursor()        
        c3.execute('SELECT COUNT(ID) FROM Genes')
        z_tup = c3.fetchall()
        z = z_tup[0]
        print "\nThere are " + str(z[0]) + " entries in the database"
        ReportGenus("genus")
        ReportSpecies("species")
    elif sys.argv[1] == "genus": 
        ReportGenus(sys.argv[2])
    elif sys.argv[1] == "species":
        ReportSpecies(sys.argv[2])

    else:
        print '''invalid entry: please enter phylo level followed by algal lineage, 
            e.g. 'genus brown' or 'species red', or 'overview' for the full dataset' '''

    \end{minted}
The output is in the form of print statements detailing the number of genera with each gene and the corresponding percentage - below is an example of the output. \\ \\*
SPECIES REPORT: red macroalgae \\ \\*
There are 3 species (8\%) that have 16S \\*
There are 17 species (45\%) that have COI \\*
There are 5 species (13\%) that have EF2 \\*
There are 16 species (43\%) that have LSU \\*
There are 17 species (45\%) that have SSU \\*
There are 4 species (10\%) that have psaA \\*
There are 21 species (56\%) that have rbcL \\*
There are 1 species (2\%) that have tufA \\*
    \section{Documentation to run the code}
    \begin{itemize}
    \item[a] \textbf {Download Get\_FASTA project from my bitbucket} \\ \\*
https://bitbucket.org/courtney\_stepien/get\_fasta
    \item[b] \textbf {Scrape Genbank for my list of genera} \\ \\*
python genbank\_get.py \textless input\_file\_with\_list\_of\_taxa.txt\textgreater \\ \\*
example: \\*
python genbank\_get.py taxa.txt
    \item[c] \textbf{Create a database} \\ \\*
python pilot\_table.py
    \item[d] \textbf{Create one file per target gene} \\ \\*
python sort.py
    \item[e] \textbf{Concatenate single-label files} together (LSU and 28S are the same gene) \\ \\*
cat secondary\_label\_for\_gene.fasta \textgreater \textgreater   primary\_label\_for\_gene.fasta \\ \\*
example: \\*
cat 28S.fasta \textgreater \textgreater LSU.fasta
    \item[f] \textbf{Format .fasta files and update database} \\ \\*
python format.py \textless single\_gene\_file.fasta \textgreater \textless output\_file.fasta \textgreater \\ \\*
example:\\*
python format.py 16S.fasta 16Sclean.fasta
    \item[g] \textbf{Query database for gene coverage} by seaweed genus, species and type (red, green, brown seaweed) \\ \\*
python query.py \textless genus or species, taxa level \textgreater \\ \\*
examples: \\*
python query.py genus brown \\*
python query.py genus green \\*
python query.py species brown \\*
python query.py overview \\*

    \end{itemize}
    \section{Results}
Based on querying the database using the input 'overview' I found the folllowing results: \\ \\*
There are 9644 entries in the database \\ \\*
GENUS REPORT: genus macroalgae \\ \\*
There are 14 genera (33\%) that have 16S \\*
There are 32 genera (76\%) that have COI \\*
There are 15 genera (35\%) that have EF2 \\*
There are 34 genera (80\%) that have LSU \\*
There are 38 genera (90\%) that have SSU \\*
There are 19 genera (45\%) that have psaA \\*
There are 41 genera (97\%) that have rbcL \\*
There are 8 genera (19\%) that have tufA \\ \\*
SPECIES REPORT: species macroalgae \\ \\*
There are 4 species (7\%) that have 16S \\*
There are 24 species (45\%) that have COI \\*
There are 5 species (9\%) that have EF2 \\*
There are 26 species (49\%) that have LSU \\*
There are 28 species (52\%) that have SSU \\*
There are 6 species (11\%) that have psaA \\*
There are 33 species (62\%) that have rbcL \\*
There are 7 species (13\%) that have tufA \\ \\*
Overall I identified 4 genes that have very high coverage at the genus level: COI, LSU, SSU and rbcL. These results are generally in agreement among the three algal lineages - red, brown and green. By using these 4 genes from my database, I have already constructed a genus-level phylogeny for my species and performed pilot analyses of phylogenetic diversity in my field seaweed communities. 

    \section{Discussion and Future Directions}
Right now there are many steps required to run the complete download and formatting, (see 8 Documentation). I could make several of these steps into functions - for example, I could make format.py a function and import it in sort.py. Additionally, the query.py command relies on reading text files of the genera and species for red, green and brown seaweeds. In the future, I will instead create dictionaries within the query.py file for the code to reference, rather than having to open and read a specific text file every time for different user input. There is room to improve the database as well: I could add additional tables for Taxonomy and other details and join these to the data table.  \\ \\*
While the scale of the project I am involved in right now is small (300-432 sequences) and doesn't necessarily require databasing or automating download from GenBank, these pipelines that I have learned are scalable. This pipeline will be very important in an upcoming collaboration I have where we will work with a much larger number of species and genes. Additionally from a career perspective, these database and python skills are transferrable to other fields and are skills that I can market for future ecology jobs.

    \section{Appendix A: Automating GenBank Downloading}
Written by R.K. Lockhart
    \begin{minted}{c}
import gzip, re, os, ftplib, math

class genbank:
	def __init__(self):
		self.keys = set([])
		self.types = ["bct","con","env","est","htc","htg","inv",
"pat","pln","pri","tsa","vrl"]
		
	def login(self):
		self.host = ftplib.FTP("ftp.bio-mirror.net")
		self.host.login("","")
		self.host.sendcmd("CWD biomirror")
		self.host.sendcmd("CWD genbank")
		
	def logout(self):
		self.host.quit()
	
	def raw_text(self,type,number):
		filename = "gb"+type.lower()+str(number)+".seq.gz"
		self.login()
		self.host.retrbinary("RETR "+filename, open(filename,"wb").write)
		file = gzip.open(filename,"rb")
		content = file.read()
		file.close()
		os.remove(filename)
		self.logout()
		return content
		
	def load(self,type,number):
		text = self.raw_text(type,number)
		text = text[text.find("\n\n\n")+3:].split("//\n")
		rec = []
		for i in text:
			this_rec = genrecord(i)
			rec.append(this_rec)
			self.keys = self.keys.union(set(this_rec.keys))
		return rec
		
	def search(self,keys, vals, types="All",numbers="All"):
		if types=="All":
			types = self.types
		elif type(types)==str:
			types = [types]
			
		if type(numbers)==int:
			numbers = [numbers]
			
		if type(vals)==str:
			vals = [vals]
			
		def write_if(i):
			for val in vals:
				if i.has_match(keys,val):
					print val, "found"
					f = open("rawgenbankdata/"+val.replace(" ","_")+
                        ".fasta", "a")
					f.write(i.fasta_form()+"\n\n")
					f.close()
					break

		for t in types:
			if numbers=="All":
				num = 1
				err = False
				while not err:
					print "searching", t, num
					try:
						[write_if(i) for i in self.load(t,num)]
						num+=1
					except ftplib.error_perm:
						err=True
			else: /*if it's a list*/
				for num in numbers:
					print "searching", t, num
					[write_if(i) for i in self.load(t,num)]
		
class genrecord:
	def __init__(self, s):
		self.record = {"REFERENCES":[]}
		self.keys = []
		s = s.replace("\n            "," ")
		parts = re.split("\n\s{0,4}(?=[A-Z]+)", s)
		re_match = re.compile("\A([A-Z]+[_a-z]*)\s+([^$]+)")
		reference_keys = ["AUTHORS","TITLE","JOURNAL","PUBMED"]
		iter = 0
		for i in parts:
			match = re_match.match(i)
			if match==None:
				continue
				
			key = match.group(1)
			val = match.group(2)
				
			if not key in self.keys:
				self.keys.append(key)
			if key=="REFERENCE":
				self.record["REFERENCES"].append({key:val})
			elif key in reference_keys:
				self.record["REFERENCES"][-1][key] = val
			elif key=="ORIGIN":
				self.record[key] = val.translate(None," 1234567890\n\t")
			else:
				self.record[key] = val
	def get(self, key, fallback=None):
		if key in self.keys:
			return self.record[key]
		else:
			return fallback
	def has_match(self, keys, val):
		truth = False
		if type(keys)==str:
			keys = [keys]
		for i in keys:
			if re.search("(?:(?<=\W)|^)"+val+"(?=\W|$)", 
                self.get(i,""), re.I)!=None:
				return True
		return False
	def fasta_form(self):
		top = self.get("VERSION","")
		top_match = re.match("(?P<gb>[^\s]+)\s+GI:(?P<gi>)",top)
		if top_match==None:
			out_str = ">gi||gb||"
		else:
			out_str = ">gi|"+top_match.group("gi")+
                "|gb|"+top_match.group("gb")+"|"
		out_str = out_str+" "+re.sub("\s+"," ",self.get("DEFINITION",""))+"\n"
		seq = self.get("ORIGIN","ERROR").upper()
		lines = int(math.floor(len(seq)/70))
		for i in range(lines):
			out_str = out_str+seq[i*70:(i+1)*70]+"\n"
		out_str = out_str+seq[lines*70:]
		return out_str

/*
#EXAMPLES FOR USE:

g = genbank()
rec = g.load("inv",1)[0]
rec.get("ORIGIN")
rec.get("REFERENCES")

rec = g.search(["DEFINITION","SOURCE","ORGANISM"], ["Synechococcus elongatus", 
    "Xanthomonas perforans"], ["bct"], [1])
print rec[0].get("DEFINITION")
*/

import genbank, sys

file = sys.argv[1]
if file!=None:
	print "READING TAXA FILE"
	gb = genbank.genbank()
	org_file = open(file,"r")
	orgs = org_file.read()
	org_file.close()
	if "\n" in orgs:
		orgs = orgs.split("\n")
	else:
		orgs = [orgs]
		
	print orgs
		
	print "SEARCHING GENBANK"
	records = gb.search(["DEFINITION","SOURCE","ORGANISM"],orgs,["pln"])
	
	/* TEST CASE records = gb.search(["DEFINITION","SOURCE","ORGANISM"],
        ["Synechococcus elongatus"],["bct"],[1]) */ 
	
	print "SUCCESS!"
	print "\a" /*makes a beeping sound*/
	
/*
USAGE EXAMPLE

> python genbank_get.py my_newline_separated_taxa_file.txt

*/
    \end{minted}

\end{document}
