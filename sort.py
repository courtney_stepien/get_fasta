
# This file is for sorting all the genes into different lists....to then format later

"""
Example of use:

python sort.py

"""

# Open a file to read, then add the contents of that file to my_string

import re

taxa_file = open('taxa.txt', 'r')
t = taxa_file.read().split("\n")
taxa_file.close()
taxa = t[0:(len(t)-1)]
print taxa

for i in range(0,len(taxa)): 
    genes = open("rawgenbankdata/" + taxa[i] + '.fasta', 'r')
    my_string = genes.read()
    genes.close()

# Split the string into a list of strings (a list of individual gene entries)

    my_list = my_string.split('\n\n')

# Define variables to keep track of how many matches, mismatches

    match_count = 0
    mismatch_count = 0
    mismatch_index = []

    def AppendToFile(labelforfile, regularexp, sequence):
        if re.search(regularexp, sequence):
            f = open("singlegene/" + labelforfile + '.fasta', 'a')
            f.write(sequence + '\n\n')
            f.close()
            print labelforfile
            global match_count
            match_count += 1


    gene_dic = {'SSU': r'\bsmall subunit\b', 'rbcL': r'\brbcL\b', 
        'ribulose': r'\bribulose\b', 'LSU': r'\blarge subunit ribosomal\b', 
        '28S': r'\b28S\b', 'cytochrome': r'\bcytochrome\b', 
        'cox1': r'\bcox1\b', 'COI': r'\bCOI\b', 'tufA': r'\btufA\b', 
        'psaA': r'\bpsaA\b', 'EF2': r'\bEF2\b', '16S': r'\b16S\b', 
        '18S': r'\b18S\b'}

    for L in range(len(my_list)):
        mis_match = match_count
        for gg in gene_dic:
            AppendToFile(gg, gene_dic[gg], my_list[L])
        if mis_match == match_count:
            mismatch_count += 1
            print 'no match' 
            mismatch_index.append(L)
        

    print 'Finished sorting genes from species ' + taxa[i] + ' for phylo analysis'
    print str(match_count) + ' genes matched out of ' + str(len(my_list)) + ' genes'
    print str(mismatch_count) + ' sequences did not match any target genes' 
    print str(match_count - len(my_list) + mismatch_count) + ' duplicate matches' 
    print 'Sequences at positions ' + str(mismatch_index) + ' do not match target genes'
