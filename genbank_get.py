import genbank, sys

file = sys.argv[1]
if file!=None:
	print "READING TAXA FILE"
	gb = genbank.genbank()
	org_file = open(file,"r")
	orgs = org_file.read()
	org_file.close()
	if "\n" in orgs:
		orgs = orgs.split("\n")
	else:
		orgs = [orgs]
		
	print orgs
		
	print "SEARCHING GENBANK"
	records = gb.search(["DEFINITION","SOURCE","ORGANISM"],orgs,["pln"])
	
	# TEST CASE #records = gb.search(["DEFINITION","SOURCE","ORGANISM"],["Synechococcus elongatus"],["bct"],[1])
	
	print "SUCCESS!"
	print "\a" #makes a beeping sound
	
"""
USAGE EXAMPLE

> python genbank_get.py my_newline_separated_taxa_file.txt

"""
