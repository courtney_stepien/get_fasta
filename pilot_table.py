# Let's make a database!

import sqlite3, csv

# create a connection to the database

conn = sqlite3.connect('AlgalSequences.db')
conn.text_factory = str

# create a 'cursor' to execute commands

c = conn.cursor()

# make the table

c.execute('''CREATE TABLE Genes
    (ID INTEGER PRIMARY KEY,
    GenbankID TEXT,    
    GenusID TEXT,    
    SpeciesID TEXT,
    TaxonomyID TEXT,
    GeneID TEXT, 
    Sequence TEXT)''')
conn.commit()

# import the taxa names into the table 

#reader = csv.reader(open('taxa_list.csv', 'rb'), delimiter = ',')
#for row in reader:
#    to_db = ((row[0]), (row[1]), (row[2]))
#    c.execute('''INSERT INTO pilot 
#    (ID, GenusID, SpeciesID) VALUES (?, ?, ?);''', to_db)
#conn.commit()

# alternate way:
#with open('taxa.csv', 'rb') as fin:
#    dr = csv.DictReader(fin)
#    to_db = [(i['ID'], i['SpeciesID']) for i in dr]

# Experiment with adding Null as fourth value in VALUES below, with GeneID in ()

#c.executemany('''INSERT INTO pilot (ID, SpeciesID)
#    VALUES (?, ?)''', to_db)
#conn.commit()

#c.execute('SELECT * FROM PILOT')
#print c.fetchall()

# Make the genus table

#c.execute('''CREATE TABLE genus
#    (ID INTEGER PRIMARY KEY,
#    GenusID TEXT,
#    Gene1 TEXT)''')

# Import my genus list into the file

#with open('genus.csv', 'rb') as fin:
#    dr = csv.DictReader(fin)
#    to_db = [(i['ID'], i['GenusID']) for i in dr]

#c.executemany('''INSERT INTO genus (ID, GenusID)
#    VALUES (?, ?)''', to_db)

#conn.commit()

#c.execute('SELECT * FROM genus')
#print c.fetchall()
